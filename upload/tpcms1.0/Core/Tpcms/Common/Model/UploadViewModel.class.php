<?php
/** [编辑器上传视图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 20:26:50
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class UploadViewModel extends ViewModel
{
	public $tableName = 'upload';

	public $viewFields  = array(
		'upload'=>array(
			'*',
			'_type'=>'INNER',
		),
		'user'=>array(
			'username','uid',
			'_type'=>'INNER',
			'_on' =>'user.uid=upload.user_uid',
		),
		
	); 
}