<?php
/** [系统设置]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 17:53:21
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:16:19
 */
namespace Admin\Controller;
class ConfigController extends PublicController{

	/**
	 * [index 所有配置]
	 * @return [type] [description]
	 */
	public function index()
	{
		if(IS_POST)
		{
			// 保存信息到数据库
			if(!$this->logic->save_config()) 
				$this->error($this->logic->getError());
			// 将配置信息写入到文件
			if(!$this->logic->write_config())
				$this->error('配置文件写入失败，请检查对应目录的权限',U('Config/index'));
			// 提示
			$this->success('系统设置更新成功',U('Config/index'));
			
		}
		else
		{
			$data = $this->logic->get_all();
			$this->assign('data',$data);
			$this->display();
		}
		
	}
		
	/**
	 * [update_cache 更新缓存]
	 * @return [type] [description]
	 */
	public function update_cache()
	{
		if(!$this->logic->write_config())
			$this->error("请检查Data文件目录权限");
		$this->success('缓存更新成功');
	}


	
}