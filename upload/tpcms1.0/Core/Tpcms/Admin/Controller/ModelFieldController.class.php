<?php
/**[字段管理]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-15 22:04:38
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 14:59:13
 */
namespace Admin\Controller;
class ModelFieldController extends PublicController
{
	private $mid;
	public function _initialize()
	{
		parent::_initialize();
		$this->mid = I('mid');
		$model = S('model');
		$this->assign('model',$model[$this->mid]);
	}
	
	/**
	 * [index 模型字段列表]
	 * @return [type] [description]
	 */
	public function index()
	{
		$data = $this->logic->get_all($this->mid);
		$this->assign('data',$data);
		$this->display();
	}
}