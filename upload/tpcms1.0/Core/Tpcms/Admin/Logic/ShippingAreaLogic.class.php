<?php
/** [配送区域表逻辑层模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-02-06 11:00:18
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class ShippingAreaLogic extends Model{
	

	
	/**
	 * [$_validate 自动验证]
	 * @var array
	 */
	protected $_validate =array(
		array('name','require','请输入配送区域名称',1)

	);

	/**
	 * [$_auto自动完成]
	 * @var array
	 */
	protected $_auto = array(

		array('shipping_shipping_id','_shipping_id',1,'callback'),
		array('code','_code',3,'callback')
	);

	protected function _shipping_id()
	{
		return I('post.shipping_id');
	}

	protected function _code()
	{
		$data = array(
			'fee'=>I('post.fee'),
			'ext'=>I('post.ext')
		);
		return serialize($data);
	}



	public function get_all($shippingId)
	{
		$data = $this->where(array('shipping_shipping_id'=>$shippingId))->select();
		
		if(!$data)
			return false;
		foreach($data as $k=>$v)
		{
			$code = unserialize($v['code']);
			$data[$k] = array_merge($v,$code);
		}
		return $data;
	}

	/**
	 * [find_one 读取一条数据]
	 * @return [type] [description]
	 */
	public function get_one($id)
	{
	
		$data   = $this->find($id);
		$code   = unserialize($data['code']);
		$data   = array_merge($data,$code);

		$area   = D('RelationShippingAreaRegion')->where(array('shipping_area_area_id'=>$id))->getField('region_region_id',true);
		$area = array_reverse($area);
		$region = array();
		$regionLogic = D('Region','Logic');
		if($area)
		{
			foreach ($area as $v) 
			{
				$temp1 = $regionLogic->get_region($v);
				$strid = array();
				if(isset($temp1['firstRegion']['region_id']))
					$strid[] = $temp1['firstRegion']['region_id'];
				if(isset($temp1['secondRegion']['region_id']))
					$strid[] = $temp1['secondRegion']['region_id'];
				if(isset($temp1['thirdRegion']['region_id']))
					$strid[] = $temp1['thirdRegion']['region_id'];
			

				$temp2 = array(
					'first'=>$regionLogic->get_same_region($temp1['firstRegion']),
					'second'=>$regionLogic->get_child_region($temp1['firstRegion']['region_id']),
					'third'=>$regionLogic->get_child_region($temp1['secondRegion']['region_id']),
				);
				$data['area'][]= array(
					'region'=>$strid,
					'area'=>$temp2,
				);
			}
		}

		return $data;
	}

	/**
	 * [del 删除]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function del($id)
	{
		D('RelationShippingAreaRegion')->where(array('shipping_area_area_id'=>$id))->delete();
		$this->delete($id);
	}


	/**
	 * [_after_insert 后置插入]
	 * @param  [type] $data    [description]
	 * @param  [type] $options [description]
	 * @return [type]          [description]
	 */
	public function _after_insert($data,$options)
	{
		$this->alter_area($data['area_id']);
	}
	/**
	 * [_after_update 后置更新]
	 * @param  [type] $data    [description]
	 * @param  [type] $options [description]
	 * @return [type]          [description]
	 */
	public function _after_update($data,$options)
	{
		$this->alter_area(I('post.area_id'));
	}


	public function alter_area($id)
	{
		$db = D('RelationShippingAreaRegion');
		$db->where(array('shipping_area_area_id'=>$id))->delete();
	
		$first  = I('post.first');
		$second = I('post.second');
		$third  = I('post.third');


		$data = array();
		foreach($first as $k=> $v)
		{
			if($third[$k])
				$data['region_region_id'] = $third[$k];
			elseif($second[$k])
				$data['region_region_id'] = $second[$k];
			elseif($first[$k])
				$data['region_region_id'] = $first[$k];
		

			$data['shipping_area_area_id'] = $id;
		
			$db->add($data);
		}

	}

	
}