<?php
/**[用户组表逻辑层]
 * @Author: chenli
 * @Email:  976123967@qq.com
 * @Date:   2015-02-23 15:41:02
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class AuthGroupLogic extends Model{

	protected $tableName ='auth_group';




	// 自动验证
	protected $_validate = array(
		array('title','require','请输入用户组名称',1),
		array('title' ,'check_title','用户组已经存在',1,'callback'),
	);
	/**
	 * [check_title 验证用户组重复]
	 * @param  [type] $con [description]
	 * @return [type]      [description]
	 */
	protected function check_title($con)
	{
		$id  = I('post.id');
		if($id)
			$where['id'] = array('neq',$id);
		$where['title'] = $con;
		$data = $this->where($where)->find();
		if($data)
			return false;
		return true;
	}

	/**
	 * [get_all 查找所有用户组]
	 * @return [type] [description]
	 */
	public function get_all()
	{
		return $this->order('id asc')->select();
	}


	/**
	 * [find_one 查找一条记录]
	 * @return [type] [description]
	 */
	public function get_one($id)
	{
		return $this->find($id);
	}

	/**
	 * [del 删除用户组]
	 * @return [type] [description]
	 */
	public function del($id)
	{
		$status = D('AuthGroupAccess')->where(array('group_id'=>$id))->getField('uid');
		if($status)
		{
			$this->error='会员组使用中';
			return false;
		}
		return ture;
	}

	/**
	 * [alter_rule 更新规则]
	 * @return [type] [description]
	 */
	public function alter_rule()
	{
		$rules = I('post.rules');
		$data['rules'] = implode(',', $rules);
		$data['id'] = I('post.id');

		$this->save($data);
		return true;
	}

	

}