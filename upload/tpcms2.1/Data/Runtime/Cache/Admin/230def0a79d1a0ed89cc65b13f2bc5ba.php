<?php if (!defined('THINK_PATH')) exit();?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><?php echo (C("cfg_name")); ?> - 提示信息</title>
<link href="/test/dwz/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/test/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" />
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/test/dwz/",
  JS_ROOT: "/test/dwz/Core/Org/"
};
</script>
<script src="/test/dwz/Core/Org/wind.js"></script>
<script src="/test/dwz/Core/Org/jquery.js"></script>
</head>
<body>
<div class="wrap">
  <div id="error_tips">
    <h2><?php echo ($msgTitle); ?></h2>
    <div class="error_cont">
      <ul>
        <li><?php echo ($message); ?></li>
      </ul>
      <div class="error_return"><a href="<?php echo ($jumpUrl); ?>" class="btn">返回</a></div>
    </div>
  </div>
</div>
<script src="/test/dwz/Core/Org/common.js"></script>
<script language="javascript">
setTimeout(function(){
	location.href = '<?php echo ($jumpUrl); ?>';
},<?php echo ($waitSecond); ?>*1000);
</script>
</body>
</html>